package figures;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Path2D;

public class Triangle extends Figure{

    private final int WIDTH = 60, HEIGHT = 60;
    public Triangle(Graphics2D buf, int del, int width, int height, AnimPanel ap) {
        super(buf, del, width, height, ap,false);

        Path2D triangle = new Path2D.Double();
        double firstX = (WIDTH / 2.0) * (1 - 1 / Math.sqrt(3));
        double firstY = 3.0 * HEIGHT / 4.0;
        triangle.moveTo(firstX, firstY);
        triangle.lineTo(WIDTH - firstX, firstY);
        triangle.lineTo(WIDTH / 2.0, HEIGHT / 4.0);
        triangle.closePath();

        shape = triangle;
        aft = new AffineTransform();
        area = new Area(shape);

    }
}
