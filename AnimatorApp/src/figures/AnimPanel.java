package figures;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.Timer;

public class AnimPanel extends JPanel implements ActionListener {

    private static final long serialVersionUID = 1L;

    //buffor
    Image image;
    //wykreslacz ekranowy
    Graphics2D device;
    //wykreslacz bufora
    Graphics2D buffer;
    private List<Figure> figures = new ArrayList<>();

    private boolean bufferRunning;

    public boolean isBufferRunning() {
        return this.bufferRunning;
    }

    private int delay = 30;

    private Timer timer;

    private static int numer = 0;

    public AnimPanel() {
        super();
        setBackground(Color.WHITE);
        timer = new Timer(delay, this);

    }

    public void initialize() {

        int width = getWidth();
        int height = getHeight();
        image = createImage(width, height);

        buffer = (Graphics2D) image.getGraphics();

        buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        device = (Graphics2D) getGraphics();

        device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    }




    void addFig() {
        Figure fig = new Square(buffer, delay, getWidth(), getHeight(), this);;
        switch (numer) {
            case 0:
                fig = new Square(buffer, delay, getWidth(), getHeight(), this);
                numer++;
                break;
            case 1:
                fig = new Ellipse(buffer, delay, getWidth(), getHeight(), this);
                numer++;
                break;
            case 2:
                fig = new Triangle(buffer, delay, getWidth(), getHeight(), this);
                numer=0;
                break;
        }
        timer.addActionListener(fig);
        new Thread(fig).start();
    }

    public void animate() {
        if (timer.isRunning()) {
            timer.stop();
            bufferRunning = false;

        } else {
            timer.start();
            bufferRunning = true;
        }
    }

    // przeniesienie bufora na ekran i wyczyszczenie go
    @Override
    public void actionPerformed(ActionEvent e) {
        buffer.setBackground(Color.WHITE);

        device.drawImage(image, 0, 0, null);

        buffer.clearRect(0, 0, getWidth(), getHeight());

    }

}
