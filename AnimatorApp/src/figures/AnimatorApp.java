package figures;
import java.awt.*;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

//Usprawnienia:
//figura - trójkąt
//tło się nie zmienia podczas animacji
//zatrzymywanie animacji - nie generują się nowe ramki
//okrąg nie wykrzywia się podczas rotacji

public class AnimatorApp extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private GridBagConstraints cs = new GridBagConstraints();

    //Launching app
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    final AnimatorApp frame = new AnimatorApp();
                    frame.setVisible(true);
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    //Creating frame
    public AnimatorApp() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        int ww = 600, wh = 600;
        setBounds((screen.width-ww)/2, (screen.height-wh)/2, ww, wh);
        setResizable(false);


        contentPane = new JPanel();
        contentPane.setBackground(Color.WHITE);
        contentPane.setLayout(new GridBagLayout());


        AnimPanel canva = new AnimPanel();
        cs.gridx = 0;
        cs.gridy = 0;
        cs.anchor = GridBagConstraints.CENTER;
        cs.gridwidth = 2;
        cs.gridheight = 4;
        cs.weightx = 0.5;
        cs.weighty = 0.9;
        cs.fill = GridBagConstraints.BOTH;
        cs.insets = new Insets(10, 10, 10, 10);
        contentPane.add(canva, cs);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                canva.initialize();
            }
        });


        cs.gridheight = 1;
        cs.gridwidth = 1;
        cs.fill = GridBagConstraints.NONE;

        JButton btnAdd = new JButton("Add");
        cs.gridx = 0;
        cs.gridy = 5;
        cs.anchor = GridBagConstraints.LAST_LINE_START;
        cs.weightx = 0.5;
        cs.weighty = 0.1;
        cs.insets = new Insets(10, 200, 30, 10);
        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canva.addFig();
            }
        });
        contentPane.add(btnAdd, cs);



        JButton btnAnimate = new JButton("Animate");
        cs.gridx = 1;
        cs.gridy = 5;
        cs.anchor = GridBagConstraints.LAST_LINE_END;
        cs.weightx = 0.5;
        cs.weighty = 0.1;
        cs.insets = new Insets(10, 10, 30, 200);
        btnAnimate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canva.animate();
            }
        });
        contentPane.add(btnAnimate, cs);

        btnAdd.setPreferredSize(btnAnimate.getPreferredSize());

        setContentPane(contentPane);

    }



}
