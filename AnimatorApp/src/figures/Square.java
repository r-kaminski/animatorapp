package figures;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RectangularShape;

public class Square extends Figure{
    public Square(Graphics2D buf, int del, int w, int h, AnimPanel ap) {
        super(buf, del, w, h, ap,false);

        shape = new Rectangle2D.Float(0, 0, 10, 10);
        aft = new AffineTransform();
        area = new Area(shape);
    }

}
