Usprawnienia dodane do AnimatorApp (4. laboratoria):

- Dodano nową figurę - trójkąt, podlegający tym samym prawom co pozostałe figury (obrót, skalowanie).
- Kanwa rysunku zachowuje swój kolor po rozpoczęciu animacji.
- Poprawiono zatrzymywanie animacji - nie generują się nowe ramki.
- Okrąg nie podlega dłużej rotacji.
